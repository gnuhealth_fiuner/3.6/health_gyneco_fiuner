#-*- coding: utf-8 -*-

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Equal, Eval, Bool, Not



__all__ = ['PatientPregnancy', 'PrenatalEvaluation']


class PatientPregnancy(metaclass = PoolMeta):
    'Obstetric History'
    
    __name__='gnuhealth.patient.pregnancy'
    
    
    height = fields.Float('Height', help='Mother height in cms')
    
    previous_pregnancies = fields.Integer('Previous Pregnancies')
    
    n_abortions = fields.Integer('Abortions')
    
    births = fields.Integer('Births')

    c_sections = fields.Integer('C-Sections')
    
    vaginal = fields.Integer('Vaginal')
    
    still_births = fields.Integer('Stillbirths')
    
    dead_births = fields.Integer('Dead Births')



class PrenatalEvaluation(metaclass = PoolMeta):    
    'Prenatal Evaluations'
    __name__ = 'gnuhealth.patient.prenatal.evaluation'
    
    patient = fields.Function(
        fields.Many2One('gnuhealth.patient','Patient'),
        'get_patient',searcher="search_patient")
    weight = fields.Float('Weight')    
    diastolic = fields.Integer('Presion Diastólica')    
    systolic = fields.Integer('Presion Sistólica')
    bmi = fields.Float('IMC',digits=(2,1))    
    fetal_movement = fields.Selection([
        (None, ''),
        ('+', '+'),
        ('-', '-'),
        ('NC', 'No corresponde'),
        ],'Fetal Movement'
        , sort=False)   

    @fields.depends('weight','name')
    def on_change_with_bmi(self):
        bmi = 00.00
        height = self.name.height
        
        if height and self.weight:
            bmi = (self.weight)/pow((float(height)/100.0),2)            
        return bmi
    
    percentil = fields.Selection([
        (None, ''),
        ('L', 'Low'),
        ('N', 'Normal'),
        ('O', 'Overweight'),
        ],'Percentil'
        , sort=False)
        
    def get_patient(self,name):
        if self.name:
            return self.name.name.id
    
    @classmethod
    def search_patient(cls,name,clause):
        res = []
        value = clause[2]
        res.append(('name.name',clause[1],value))
        return res
    
    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('patient.rec_name',) + tuple(clause[1:]),           
            ]
