from trytond.pool import Pool
from .health_gyneco_fiuner import *

def register():
    Pool.register(
        PatientPregnancy,
        PrenatalEvaluation,
        module='health_gyneco_fiuner', type_='model')
